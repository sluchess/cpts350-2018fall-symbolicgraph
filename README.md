# SymbolicGraph in Python3

Project description from instructor:


In this project, you are going to code in Python using pyEDA package to


a.  translate a graph into a Boolean formula (represented by BDD), and


b.  verify whether a given property is satisfied for the graph using the BDD.
        Let S be the set {0,···,31}.  We use i%32 to the number j in S such that
    i = j+k·32 for some k.  We now define a graph G with 32 nodes, satisfying that,
    for all i, j ∈ S,
        there is an edge from node i to node j iff (i+3)%32 = j%32 or (i+7)%32 = j%32.


Step 1.  Build a BDD F to represent the graph G, using some methods in
the Python package.


Step 2.  Using some methods provided with the Python package,  compute
the transitive closure F∗ (see your class notes), which is again a BDD.


Step 3.  We are interested in whether, for all i, j ∈ S, node i can reach node j
in one or more steps in G.  We denote the property as P. From the BDD F∗, you need
further compute and return yes/no on whether P is true