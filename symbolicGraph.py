"""CptS350 Symbolic graph project
In this project, you are going to code in Python using pyEDA package to

a.  translate a graph into a Boolean formula (represented by BDD), and

b.  verify whether a given property is satisfied for the graph using the BDD.
        Let S be the set {0,···,31}.  We use i%32 to the number j in S such that
    i = j+k·32 for some k.  We now define a graph G with 32 nodes, satisfying that,
    for all i, j ∈ S,
        there is an edge from node i to node j iff (i+3)%32 = j%32 or (i+7)%32 = j%32.

Step 1.  Build a BDD F to represent the graph G, using some methods in
the Python package.

Step 2.  Using some methods provided with the Python package,  compute
the transitive closure F∗ (see your class notes), which is again a BDD.

Step 3.  We are interested in whether, for all i, j ∈ S, node i can reach node j
in one or more steps in G.  We denote the property as P. From the BDD F∗, you need
further compute and return yes/no on whether P is true"""

from pyeda.boolalg.expr import *
from pyeda.boolalg.bfarray import *
from pyeda.inter import *
from pyeda.boolalg.bdd import BDDONE
import math


# Finds the valid edges of the graph, adds each one to a tuple, and then appends the tuple to a list.
# Returns the list of all valid edges in tuples.
def find_edges():
    valid_edges = []
    for i in range(32):
        for j in range(32):
            if (((i+3) % 32) == (j % 32)) or (((i+7) % 32) == (j % 32)):
                if (i, j) not in valid_edges:
                    valid_edges.append((i, j))
    # print("Valid edges:")
    # print(valid_edges)
    return valid_edges


# Takes all tuples from find_edges() and separates the i values and j values into separate lists.
# Returns both lists of valid i and j edge values.
def create_lists():
    i_list = []
    j_list = []
    valid_edges = find_edges()
    for x in range(len(valid_edges)):
        i_list.append(valid_edges[x][0])  # first item of every tuple is the i value
        j_list.append(valid_edges[x][1])  # second item of every tuple is the j value
    # print("i_list:")
    # print(i_list)
    # print("j_list:")
    # print(j_list)
    return i_list, j_list


# Takes both lists of values from create_lists() and converts all values in each list to their respective
# binary representation. Returns the two new lists.
def convert_to_bin():
    i_list, j_list = create_lists()
    for x in range(len(i_list)):  # this applies to both list since they are the same length
        i_list[x] = (bin(i_list[x])[2:])  # remove the 0b that bin() adds to the beginning of the binary number
        j_list[x] = (bin(j_list[x])[2:])
    # print("i_list:")
    # print(i_list)
    # print("j_list:")
    # print(j_list)
    return i_list, j_list


# Adds zeroes to the beginning of all binary numbers in the lists from convert_to_bin()
# if they are less than 5 digits long. Returns the new lists.
def add_zeroes():
    temp = ""
    i_list, j_list = convert_to_bin()
    for x in range(len(i_list)):  # this applies to both list since they are the same length
        while (len(temp) + len(i_list[x])) < 5:
            temp += "0"
        temp += i_list[x]
        i_list[x] = temp
        temp = ""
        while (len(temp) + len(j_list[x])) < 5:
            temp += "0"
        temp += j_list[x]
        j_list[x] = temp
        temp = ""
    # print("i_list:")
    # print(i_list)
    # print("j_list:")
    # print(j_list)
    return i_list, j_list


# Takes the pairs from each list from add_zeroes() and appends them together so that they form one large binary
# string. Returns the list of all these string.
def append_bin():
    b_list = []
    i_list, j_list = add_zeroes()
    for x in range(len(i_list)):  # this applies to both list since they are the same length
        b_list.append(i_list[x] + j_list[x])
    # print("b_list:")
    # print(b_list)
    return b_list


# converts the binary strings to strings readable expr()
def create_expr():
    count = 0
    temp = ""
    expr_list = []
    b_list = append_bin()
    for x in range(len(b_list)):
        while count < 5:
            if (b_list[x])[count] == "0":
                temp += "~"
            temp += "i" + str(count) + " & "
            count += 1
        while count < 10:
            if (b_list[x])[count] == "0":
                temp += "~"
            temp += "j" + str(count - 5) + " & "
            count += 1
        temp = temp[:-1]
        temp = temp[:-1]
        expr_list.append(temp[:-1])
        temp = ""
        count = 0
    # print("expr_list:")
    # print(expr_list)
    return expr_list


# converts list of boolean expressions to a BDD list
def create_bdd_list():
    bdd_list = []
    expr_list = create_expr()
    for x in expr_list:
        bdd_list.append(expr2bdd(expr(x)))
    # print("bdd_list:")
    # print(bdd_list)
    return bdd_list


# converts bdd_list to BDDR
def create_bddr():
    bdd_list = create_bdd_list()
    bddr = bdd_list[0]
    for x in range(1, len(bdd_list)):
        bddr = expr2bdd(bddr | bdd_list[x])
    # print("bddr:")
    # print(bddr)
    return bddr


# # main function
if __name__ == "__main__":
    # x,y,z = map(expr, "xyz")
    # str1 = "i0,i1,i2,i3,i4"
    # i0, i1, i2, i3, i4 = map(bddvar, str1.split(','))
    # str1 = "j0,j1,j2,j3,j4"
    # j0, j1, j2, j3, j4 = map(bddvar, str1.split(','))
    # str1 = "k0,k1,k2,k3,k4"
    # k0, k1, k2, k3, k4 = map(bddvar, str1.split(','))

    ii,jj,kk = map(bddvar, "ijk")

    bddr = create_bddr()

    # r1 = bddr.compose({i0: k0, i1: k1, i2: k2, i3: k3, i4: k4})
    r1 = bddr.compose({ii:kk})
    # r2 = bddr.compose({k0: j0, k1: j1, k2: j2, k3: j3, k4: j4})
    r2 = bddr.compose({kk:jj})
    bddr = not (r1 & r2).smoothing(kk).equivalent(False)
    print(bddr)


# end of file
